import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 20px;
  min-height: 100vh;
`;

export { Wrapper };
