import { useState, useEffect } from 'react';
import { Row, Col } from 'antd';

import Button from '../components/CustomUI/button';
import notification from '../components/CustomUI/notification';
import { axiosInstance } from '../helper/base-http.service';
import List from '../components/Order/List';
import Detail from '../components/Order/Detail';
import { Wrapper } from './Order.Style';

const index = () => {
  const [selectedRecord, setSelectedRecord] = useState(null);
  const [loading, setLoading] = useState(true);
  const [saveLoading, setSaveLoading] = useState(false);
  const [searchText, setSearchText] = useState(null);
  const [searchedColumn, setSearchColumn] = useState(null);
  const [dataSource, setDataSource] = useState([]);
  let [pagination, setPagination] = useState({
    totalItems: 0,
    itemCount: 0,
    itemsPerPage: 10,
    totalPages: 0,
    currentPage: 1,
  });
  const [filterOrderNo, setFilterOrderNo] = useState(null);
  const [filterStatus, setFilterStatus] = useState(null);
  const [filterRemark, setFilterRemark] = useState(null);
  const [sort, setSort] = useState({
    field: 'createdAt',
    order: 'DESC',
  });

  useEffect(() => {
    const pageLoad = async () => {
      await loadData();
    };

    pageLoad();

    return () => {};
  }, []);

  useEffect(() => {
    loadData();
  }, [sort]);

  const loadData = async () => {
    setLoading(true);
    try {
      const params = {
        limit: pagination.itemsPerPage,
        page: pagination.currentPage,
        orderBy: sort.field,
        sortBy: sort.order,

        filterOrderNo,
        filterRemark,
        orderStatus: filterStatus,
      };

      const dataList = await axiosInstance.get(`/orders`, {
        params,
      });

      const { data } = dataList;
      const { items, meta } = data;

      setPagination(meta);

      setDataSource(items);
    } catch (e) {
      console.log('loadData', e);
    }
    setLoading(false);
  };

  const onSelectRecordHandler = async (id) => {
    setLoading(true);
    try {
      const response = await axiosInstance.get(`/orders/${id}`);
      const { data } = response;
      setSelectedRecord(data);
    } catch (e) {
      console.log('onSelectRecordHandler', e);
    }
    setLoading(false);
  };

  const tableOnChangeHandler = (paginate, filters, sorter) => {
    setPagination({
      totalItems: paginate.total,
      itemCount: paginate.pageSize,
      itemsPerPage: paginate.pageSize,
      totalPages: 0,
      currentPage: paginate.current,
    });

    setSort({
      field: sorter.field,
      order: !!sorter.order
        ? sorter.order.replace('end', '').toUpperCase()
        : 'DESC',
    });

    setFilterOrderNo(null);
    setFilterStatus(null);
    setFilterRemark(null);

    for (const filter in filters) {
      if (!!filters[filter]) {
        let type = typeof filters[filter][0];
        if (type === 'string') {
          if (filter === 'orderNo') {
            setFilterOrderNo(filters[filter][0]);
          }
          if (filter === 'status') {
            setFilterStatus(filters[filter][0]);
          }
          if (filter === 'remark') {
            setFilterRemark(filters[filter][0]);
          }
        }
      }
    }
  };

  const onAddNewButtonClickHandler = () => {
    setSelectedRecord({
      amount: 0.0,
    });
  };

  const onSubmitHandler = async (value) => {
    setSaveLoading(true);
    try {
      const response = await axiosInstance.post(`/orders`, {
        ...value,
      });
      if ([200, 201].includes(response.status)) {
        setSelectedRecord(null);
        notification('success', 'New Order Created');
        loadData();
      }
    } catch (err) {
      if (err && err.response && err.response.data) {
        notification('error', err.response.data.message);
      }
    }
    setSaveLoading(false);
  };

  const onCancelOrderHandler = async () => {
    setSaveLoading(true);
    try {
      const response = await axiosInstance.patch(
        `/orders/${selectedRecord.id}/status`
      );

      if ([200].includes(response.status)) {
        setSelectedRecord(null);
        notification('success', 'Order cancelled');
      }
    } catch (err) {
      if (err && err.response && err.response.data) {
        notification('error', err.response.data.message);
      }
    }
    setSaveLoading(false);
  };

  return (
    <Wrapper>
      <Row gutter={[16, 16]}>
        <Col>
          <Button onClick={() => onAddNewButtonClickHandler()}>
            Create Order
          </Button>
        </Col>
        <Col>
          <div>
            Confirmed order need to wait after 60 seconds of confirmed date to
            change the status to delivered
          </div>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <List
            paginate={pagination}
            dataSource={dataSource}
            loading={loading}
            tableOnChangeHandler={tableOnChangeHandler}
            onSelectRecordHandler={(order) => onSelectRecordHandler(order.id)}
            filterHandleSearch={(selectedKeys, dataIndex) => {
              setSearchText(selectedKeys[0]);
              setSearchColumn(dataIndex);
            }}
            filterHandleClear={() => {
              setSearchText(null);
            }}
          />
        </Col>

        {!!selectedRecord && (
          <Detail
            title={selectedRecord.id ? 'View Order' : 'Create Order'}
            saveLoading={saveLoading}
            selectedRecord={selectedRecord}
            onCloseModalHandler={() => setSelectedRecord(null)}
            onSubmitHandler={onSubmitHandler}
            onCancelOrderHandler={onCancelOrderHandler}
          />
        )}
      </Row>
    </Wrapper>
  );
};

export default index;
