import { Table } from 'antd';

const CustomTableUI = (props) => {
  return (
    <Table
      size={'small'}
      borde
      ellipsis
      rowKey={(record) => record.id}
      {...props}
    />
  );
};

export default CustomTableUI;
