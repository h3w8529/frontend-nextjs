import { Button } from 'antd';

const CustomButtonUI = (props) => {
  return <Button shape="round" {...props} type="primary" />;
};

export default CustomButtonUI;
