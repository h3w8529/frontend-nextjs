import { notification } from 'antd';

const createNotification = (
  type = 'info',
  message = '',
  description = '',
  duration = 4.5
) => {
  try {
    notification[type.toLowerCase()]({
      message: message,
      description: description,
      duration,
    });
  } catch (e) {
    console.error(e);
  }
};
export default createNotification;
