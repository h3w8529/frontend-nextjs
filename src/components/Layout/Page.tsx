import { withRouter } from 'next/router';

import { Layout } from 'antd';
const { Header, Footer, Sider, Content } = Layout;

const Page = ({ router, children }) => {
	return (
		<Layout>
			<Header style={{ color: '#ffffff' }}>Hew's Order Management Quiz</Header>
			{children}
		</Layout>
	);
};

export default withRouter(Page);
