import { Select, Input } from 'antd';
import { useState } from 'react';
import { EllipsisOutlined } from '@ant-design/icons';
import moment from 'moment';
import { SearchOutlined } from '@ant-design/icons';

import {
  DEFAULT_DATETIME_FORMAT,
  ORDER_STATUS_LIST,
} from '../../helper/Constants';

import Table from '../CustomUI/table';
import Button from '../CustomUI/button';

import { Wrapper } from './List.Style';

const list = (props) => {
  const {
    paginate,
    onSelectRecordHandler,
    tableOnChangeHandler,
    filterHandleSearch,
    filterHandleClear,
  } = props;

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    filterHandleSearch(selectedKeys, dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    filterHandleClear();
  };

  const getColumnSearchProps = (dataIndex, searchPlaceholder) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => {
      let searchInput = null;
      if (dataIndex === 'status') {
        return (
          <div style={{ padding: 8 }}>
            <Select
              style={{ width: '50vh' }}
              placeholder="Order Status"
              optionFilterProp="children"
              showSearch
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              onChange={(value) => {
                setSelectedKeys([value]);
                handleSearch(selectedKeys, confirm, dataIndex);
              }}
            >
              {ORDER_STATUS_LIST.map((item) => (
                <Select.Option key={item} value={item}>
                  {item}
                </Select.Option>
              ))}
              <Select.Option value={null}>ALL</Select.Option>
            </Select>
          </div>
        );
      }
      return (
        <div style={{ padding: 8 }}>
          <Input
            ref={(node) => {
              searchInput = node;
            }}
            placeholder={`Search ${searchPlaceholder}`}
            value={selectedKeys[0]}
            onChange={(e) =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
            style={{
              width: 188,
              marginBottom: 8,
              display: 'block',
            }}
          />
          <Button
            type="primary"
            size="small"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            style={{ width: 90, marginRight: 8 }}
          >
            Search
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </div>
      );
    },
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    sorter: true,
  });

  const columns = [
    {
      title: '',
      dataIndex: 'id',
      key: 'id',
      width: '50px',
      render: (text, record) => (
        <span>
          <div>
            <Button
              shape="circle"
              onClick={() => {
                onSelectRecordHandler(record);
              }}
              style={{ marginRight: '10px' }}
              icon={<EllipsisOutlined />}
            />
          </div>
        </span>
      ),
    },
    {
      title: 'Order No',
      dataIndex: 'orderNo',
      key: 'orderNo',
      sorter: true,
      ...getColumnSearchProps('orderNo', 'Order No'),
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      sorter: true,
      ...getColumnSearchProps('status', 'Order Status'),
    },
    {
      title: 'Remark',
      dataIndex: 'remark',
      key: 'remark',
      responsive: ['md'],
      sorter: true,
      ...getColumnSearchProps('remark', 'Remark'),
    },
    {
      title: 'Amount (RM)',
      dataIndex: 'amount',
      key: 'amount',
      responsive: ['md'],
      width: '150px',
      sorter: true,
      render: (text, record) => (
        <span style={{ float: 'right' }}>{parseFloat(text).toFixed(2)}</span>
      ),
    },

    {
      title: 'Creation Date',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (text) => moment(text).format(DEFAULT_DATETIME_FORMAT),
      responsive: ['md'],
      sorter: true,
    },
  ];

  return (
    <Wrapper>
      <Table
        columns={columns}
        onChange={tableOnChangeHandler}
        pagination={{
          total: !!paginate ? paginate.totalItems : 0,
          showTotal: (total, range) =>
            `${range[0]}-${range[1]} of ${total} items`,
          pageSize: !!paginate ? paginate.itemsPerPage : 10,
          defaultCurrent: 1,
        }}
        {...props}
      />
    </Wrapper>
  );
};

export default list;
