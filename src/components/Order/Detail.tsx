import { Modal, Form, Input, InputNumber } from 'antd';
import moment from 'moment';

import { DEFAULT_DATETIME_FORMAT } from '../../helper/Constants';
import Button from '../CustomUI/button';

import { Wrapper } from './Detail.Style';

const layout = {
  labelCol: {
    span: 6,
  },
};

const detail = (props) => {
  const {
    title = 'Create Order',
    saveLoading,
    selectedRecord,
    onSubmitHandler = () => {},
    onCloseModalHandler = () => {},
    onCancelOrderHandler = () => {},
  } = props;

  if (selectedRecord.createdAt) {
    selectedRecord.createdAt = moment(selectedRecord.createdAt).format(
      DEFAULT_DATETIME_FORMAT
    );
  }
  if (selectedRecord.confirmedAt) {
    selectedRecord.confirmedAt = moment(selectedRecord.confirmedAt).format(
      DEFAULT_DATETIME_FORMAT
    );
  }
  if (selectedRecord.paymentAt) {
    selectedRecord.paymentAt = moment(selectedRecord.paymentAt).format(
      DEFAULT_DATETIME_FORMAT
    );
  }
  if (selectedRecord.cancelledAt) {
    selectedRecord.cancelledAt = moment(selectedRecord.cancelledAt).format(
      DEFAULT_DATETIME_FORMAT
    );
  }
  if (selectedRecord.deliveredAt) {
    selectedRecord.deliveredAt = moment(selectedRecord.deliveredAt).format(
      DEFAULT_DATETIME_FORMAT
    );
  }

  const generateViewOnlyData = () => {
    return (
      <div>
        <Form.Item name="status" label="Order Status">
          <Input readOnly />
        </Form.Item>

        <Form.Item name="createdAt" label="Creation Date">
          <Input readOnly />
        </Form.Item>
        {selectedRecord.confirmedAt && (
          <Form.Item name="confirmedAt" label="Confirmation Date">
            <Input readOnly />
          </Form.Item>
        )}
        {selectedRecord.paymentAt && (
          <Form.Item name="paymentAt" label="Payment Date">
            <Input readOnly />
          </Form.Item>
        )}
        {selectedRecord.cancelledAt && (
          <Form.Item name="cancelledAt" label="Cancellation Date">
            <Input readOnly />
          </Form.Item>
        )}
        {selectedRecord.deliveredAt && (
          <Form.Item name="deliveredAt" label="Delivery Date">
            <Input readOnly />
          </Form.Item>
        )}
      </div>
    );
  };
  return (
    <Modal
      width={1000}
      title={title}
      visible={!!selectedRecord}
      onCancel={() => onCloseModalHandler()}
      footer={null}
    >
      <Wrapper>
        <Form
          {...layout}
          name="orderForm"
          onFinish={onSubmitHandler}
          initialValues={selectedRecord}
        >
          <Form.Item
            name="remark"
            label="Order Remark"
            rules={[
              {
                required: true,
                message: 'Remark is required!',
              },
            ]}
          >
            <Input readOnly={selectedRecord.id} placeholder="Remark" />
          </Form.Item>

          <Form.Item
            name="amount"
            label="Amount"
            rules={[{ required: true, message: 'Amount is required!' }]}
          >
            <InputNumber
              readOnly={selectedRecord.id}
              style={{ width: '100%' }}
              placeholder="RM 1.00"
              precision={2}
              min={0}
              step={0.1}
              formatter={(value) =>
                `RM ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
              }
              parser={(value) => value.replace(/RM\s?|(,*)/g, '')}
            />
          </Form.Item>

          {selectedRecord.id && generateViewOnlyData()}

          {!selectedRecord.id && (
            <Form.Item style={{ textAlign: 'right' }}>
              <Button type="primary" htmlType="submit" loading={saveLoading}>
                Create
              </Button>
            </Form.Item>
          )}

          {selectedRecord.id && selectedRecord.status === 'CONFIRMED' && (
            <Form.Item style={{ textAlign: 'right' }}>
              <Button
                type="primary"
                danger
                loading={saveLoading}
                onClick={onCancelOrderHandler}
              >
                Cancel Order
              </Button>
            </Form.Item>
          )}
        </Form>
      </Wrapper>
    </Modal>
  );
};

export default detail;
