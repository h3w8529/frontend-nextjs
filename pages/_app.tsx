import '../src/components/Styles/styles.less';

import React from 'react';
import Head from 'next/head';
import { AppProps } from 'next/app';
import { GlobalStyles } from '../src/components/Styles/GlobalStyles';
import Page from '../src/components/Layout/Page';

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <>
      <GlobalStyles />
      <Head>
        <title>{process.env.pageTitle}</title>
      </Head>
      <Page>
        <Component {...pageProps} />
      </Page>
    </>
  );
};

export default MyApp;
