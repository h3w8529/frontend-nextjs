const withPlugins = require('next-compose-plugins');
const withLess = require('@zeit/next-less');
const nextConfig = {
  pwa: {
    dest: 'public',
    register: false, // production set to true
    disable: true, // production set to false
  },
  env: {
    pageTitle: 'Order Management Quiz',
    apiUrl: 'https://setel-quiz.hewfuikhien.com',
  },
  onDemandEntries: {
    maxInactiveAge: 1000 * 60 * 60,
    pagesBufferLength: 5,
  },
  lessLoaderOptions: {
    javascriptEnabled: true,
  },
  transpileModules: [
    'bs-platform',
    'bs-css',
    'reason-apollo-hooks',
    're-formality',
  ],
  pageExtensions: ['tsx'],
  resolve: {
    modules: ['sass_loader'],
    cssModules: true,
  },
  webpack(config, options) {
    config.module.rules.push({
      test: /\.css$/,
      loader: [require.resolve('postcss-loader')],
    });

    config.module.rules.push({
      test: /\.(png|jpe?g|gif|svg)$/i,
      loader: 'file-loader',
      options: {
        outputPath: 'images',
      },
    });
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty',
    };
    return config;
  },
};

module.exports = withPlugins([withLess], nextConfig);
